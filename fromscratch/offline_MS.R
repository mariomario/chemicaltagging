library("viridis")
library("tsne")
library("dbscan")
library("cluster")
library("mclust")
library("dbscan")
library("umap")
library("C50")
library("e1071")
library("sparsepca")
library("biclust")
set.seed(68)

#scartare: gamma2 Vel, 

anto <- read.csv("OC_UVES_iDR5.csv")
iniges <- table(anto$GES_FLD)

young <- c("lam_Ori", "gamma2_Vel", "NGC2233", "IC2602", "IC2391", "Cha_I") 
pdf("checkGiants.pdf")
clean <- anto$TEFF < 8000
plot(anto$TEFF[clean], anto$LOGG[clean], pch = 16, cex = 0.5, xlim = rev(range(anto$TEFF[clean])), ylim = rev(range(anto$LOGG[clean])), xlab = expression(T["eff"]), ylab = "log g", col = "#A0A0AA")
points((anto$TEFF[clean])[as.character(anto$GES_FLD[clean]) == "NGC2243"], (anto$LOGG[clean])[as.character(anto$GES_FLD[clean]) == "NGC2243"], col = "orange")

#sapply(1:length(young), function(i) {
#    points((anto$TEFF[clean])[iniges[clean] %in% young[i]], (anto$LOGG[clean])[iniges[clean] %in% young[i]], col = rainbow(length(young))[i], cex = 1.5)
#    return(length((anto$TEFF[clean])[iniges[clean] %in% young[i]]))
#})

pdf("selection.pdf")
clean <- anto$TEFF < 8000
plot(anto$TEFF[clean], anto$LOGG[clean], pch = 16, cex = 0.5, xlim = rev(range(anto$TEFF[clean])), ylim = rev(range(anto$LOGG[clean])), xlab = expression(T["eff"]), ylab = "log g", col = "#A0A0AA")


hr <- data.frame(anto$LOGG[clean], anto$TEFF[clean]/1000)
dbscanhr <- dbscan(hr, eps=0.3, minPts=50)
dbscanhr
summary(dbscanhr)
dbscanclu <- dbscanhr$cluster
points(anto$TEFF[clean], anto$LOGG[clean], col = "#A0A0A0", pch = 16, cex = 0.7)
giants <- dbscanclu == 1 #max(dbscanclu)
points((anto$TEFF[clean])[giants], (anto$LOGG[clean])[giants], pch = 16, cex = 0.8, col = "#FC7001")

selected_cluster <- "IC2602"

points((anto$TEFF[clean])[as.character(anto$GES_FLD[clean]) == selected_cluster], (anto$LOGG[clean])[as.character(anto$GES_FLD[clean]) == selected_cluster], col = "#00AAFF")
summary(anto[as.character(anto$GES_FLD[clean]) == selected_cluster,])

selc <- anto[as.character(anto$GES_FLD[clean]) == selected_cluster,]

makecolorpalette <- rainbow #viridis #

lighten <- function(color)
{
	colinrgb <- col2rgb(color)
	colinhsv <- rgb2hsv(colinrgb[1,], colinrgb[2,], colinrgb[3,], maxColorValue=255)
	lightcolinrgb <- hsv(h=colinhsv[1,], s=0.3*colinhsv[2,], v=colinhsv[3,])
    return(lightcolinrgb)
}

anto <- (anto[clean,])
nrow(anto) #how many stars initially
anto <- anto[giants,] #keep only giant stars
nrow(anto) #how many giant stars

anto <-  anto[!is.na(anto$VRAD),] #keep only stars with measured radial velocity
nrow(anto) #how many giant stars with radial velocity

#fields that, after the selection, still contain more than a given number of stars
names(table(anto$GES_FLD))
populated_fields <- names(table(anto$GES_FLD)[table(anto$GES_FLD) > 10])
populated_fields
anto <- anto[as.character(anto$GES_FLD) %in% populated_fields,]

table(as.character(anto$GES_FLD))
nrow(anto) #how many stars after throwing away sparsely populated fields

#check that we really did keep only stars with measured radial velocity
if(anyNA(anto$VRAD)) stop("NAs in radial velocity left after removing them...")

medianvel <- tapply(anto$VRAD, as.factor(as.character(anto$GES_FLD)), median) #median
madvel <- tapply(anto$VRAD, as.factor(as.character(anto$GES_FLD)), mad) #mad

#medianSC1 <- tapply(anto$SC1[!is.na(anto$SC1)], as.factor(as.character(anto$GES_FLD)), median) #median
#madSC1 <- tapply(anto$SC1[!is.na(anto$SC1)], as.factor(as.character(anto$GES_FLD)), mad) #mad
medianFE1 <- tapply(anto$FE1[!is.na(anto$FE1)], as.factor(as.character(anto$GES_FLD)[!is.na(anto$FE1)]), median) #median
madFE1 <- tapply(anto$FE1[!is.na(anto$FE1)], as.factor(as.character(anto$GES_FLD)[!is.na(anto$FE1)]), mad) #mad
medianFE2 <- tapply(anto$FE2[!is.na(anto$FE2)], as.factor(as.character(anto$GES_FLD)[!is.na(anto$FE2)]), median) #median
madFE2 <- tapply(anto$FE2[!is.na(anto$FE2)], as.factor(as.character(anto$GES_FLD)[!is.na(anto$FE2)]), mad) #mad


field_names <- names(medianvel)

field_names
length(field_names)

coolorscodes <- sapply(as.character(anto$GES_FLD), function(cha) (1:length(field_names))[field_names == cha])
coolors <- makecolorpalette(max(coolorscodes))[coolorscodes]
fields <- field_names[coolorscodes]
pchs <- rep(c(0:6, 8, 15:18),2)[coolorscodes]

velocity_selected <- abs(anto$VRAD - medianvel[coolorscodes]) < 1.5*madvel[coolorscodes]
#SC1_selected <- abs(anto$SC1 - medianSC1[coolorscodes]) < 2.0*madSC1[coolorscodes]
FE1_selected <- abs(anto$FE1 - medianFE1[coolorscodes]) < 3.0*madFE1[coolorscodes]
FE2_selected <- abs(anto$FE2 - medianFE2[coolorscodes]) < 3.0*madFE2[coolorscodes]
metallicity_selected <- FE1_selected & FE2_selected

table(metallicity_selected)

#velocity_selected <- velocity_selected & metallicity_selected 

coolors[!velocity_selected] <- "#A0A0A0" #lighten(coolors[!velocity_selected])

polish_names <- function(uglynames)
{
    polishednames <- gsub("NGC", "NGC ", uglynames)
    polishednames <- gsub("Br", "Br ", polishednames)
    polishednames <- gsub("IC", "IC ", polishednames)
    polishednames <- gsub("Rup", "Rup ", polishednames)
    polishednames <- gsub("Trumpler", "Trumpler ", polishednames)
    polishednames <- gsub("_", " ", polishednames)
    return(polishednames)
}

pdf("VradDiagnostics.pdf")
dotchart(anto$VRAD, groups=as.factor(polish_names(as.character(anto$GES_FLD))), gdata = medianvel, gpch = "", pch = 16, pt.cex = 0.5, lcolor = "white", col = coolors, xlab = "Radial velocity")

nrow(anto)
anto <- anto[velocity_selected,]
nrow(anto)

coolorscodes <- coolorscodes[velocity_selected]
coolors <- coolors[velocity_selected]
fields <- fields[velocity_selected]
pchs <- pchs[velocity_selected]

table(as.character(anto$GES_FLD))

#definiamo gli elementi
element_names <- names(anto)[nchar(names(anto)) < 4][7:66]
elements <- anto[, element_names]

#calcoliamo quanti NA ha ciascun elemento
nafrac <- function(u) {length(u[is.na(u)])/length(u)}

nafraccutoff <- 0.3

element_NA_fraction <- apply(elements, 2, nafrac)
pdf("complete_elements.pdf")
dotchart(1.0-sort((element_NA_fraction)[element_NA_fraction < 1]), pch = 16, xlab = "Fraction with measured abundance")
lines(1 - c(nafraccutoff,nafraccutoff), c(-100,100))

#teniamo solo quelli con NA fraction < nafraccutoff
decently_complete_element_names <- names(element_NA_fraction[element_NA_fraction < nafraccutoff]) #at least 80% complete
decently_complete_element_names

decently_complete_elements <- elements[,decently_complete_element_names]
elements <- decently_complete_elements

head(elements)

#escludiamo alcuni elementi per ragioni astrofisiche
angela_excluded_elements <- c("NA1", "LI1", "C1") #sodio in stelle massicce varia al dredge up
elements <- elements[,!(names(elements) %in% angela_excluded_elements)]

#teniamo solo le stelle che hanno misure di tutti gli elementi selezionati
(sapply(1:nrow(elements), function(i) !anyNA(elements[i,]))) -> completestars
table(completestars)

cs_elements <- elements[completestars,]
cs_coolorscodes <- coolorscodes[completestars]
cs_coolors <- coolors[completestars]
cs_fields <- fields[completestars]
cs_pchs <- pchs[completestars]

udf <- unique(data.frame(cs_fields, cs_pchs, cs_coolors))


nrow(cs_elements)
ncol(cs_elements)
names(cs_elements)
data.frame(iniges, table(anto$GES_FLD), -iniges + table(anto$GES_FLD))

#prova sottrazine Y2 MG1 ... solo per MS
cs_elements$Y2 <- cs_elements$Y2 - cs_elements$MG1

pdf("NGC6253.pdf")
plot(selc$FEH, selc$VRAD)
points(selc$FEH[giants], selc$VRAD[giants], pch = 16)

#pdf("tsne.pdf")
#plot(tsne(cs_elements, perplexity = 5), col = cs_coolors, pch = cs_pchs)


pdf("permutationimportanceandcrosspredictability.pdf")
library("iml")
treemodel <- svm(as.factor(cs_fields) ~ ., data = cs_elements, cost = 100)
predictor <- Predictor$new(treemodel, data = cs_elements, y = cs_fields)
imp <- FeatureImp$new(predictor, loss = "ce")

imp$results

plot(imp)

element_importance <- imp$results[,3]

template <- "summary(lm(placeholder ~ ., data = cs_elements))$adj.r.squared"
lmi <- function(i) eval(parse(text = gsub("placeholder",imp$results[i,1],template)))

crosspredictability <- sapply(1:nrow(imp$results), lmi)

data.frame(imp$results[,1], crosspredictability, imp$results[,3]) #the lower the better

plot(crosspredictability, imp$results[,3], pch = 16, cex = 0.7, xlab = "cross predictability (adj. R squared)", ylab = "Permutation importance", type = "n")
text(crosspredictability, imp$results[,3], imp$results[,1], col="#A0A0A0", cex = 0.5)

useful <- imp$results[imp$results[,4] > quantile(imp$results[,4], 0.0),1]
useful

dev.off()

unique(cs_coolors)
ucs_pchs <- rep(c(0:6, 8, 15:18),2)[1:length(unique(cs_coolorscodes))]
unique(cs_fields)
ucs_pchs

data.frame(unique(cs_fields), unique(cs_coolors), ucs_pchs)

pdf("tsne_useful.pdf")
tu <- tsne(cs_elements[,useful], perplexity = 5)
force_scale <- FALSE
xlim_f <- NULL
if(force_scale) xlim_f <- c(-300, 300)
plot(tu, col = lighten(cs_coolors), pch = cs_pchs, cex = 0.7, xlim = xlim_f, ylim = xlim_f)
legend(x="bottomleft", legend=udf[,1], col=lighten(udf[,3]), cex = 0.655, pch = udf[,2])

aridbscan <- function(minPts, data, labels, pcut, fields=cs_fields)
{
    hdbresults <- hdbscan(data, minPts = minPts)
    dbscangroups <- hdbresults$cluster
    notnoise <- hdbresults$membership_prob > pcut
    ari <- adjustedRandIndex(dbscangroups[notnoise], labels[notnoise])
    diagnos <- c(ari, length(dbscangroups[notnoise])/length(dbscangroups), length(unique(labels[notnoise])), min(dbscangroups[notnoise]), length(unique(dbscangroups[notnoise])))
    return(list(diagnos=diagnos, tab = table(dbscangroups[notnoise], fields[notnoise])))
}

arirando <- function(k, N, l)
{
    labels1 <- sample(1:k, l, replace = TRUE)
    labels2 <- sample(1:N, l, replace = TRUE)
    adjustedRandIndex(labels1, labels2)
}

sort(sapply(1:1000, function(i) arirando(15, 17, nrow(tu))))
aridbscan(2, cs_elements[,useful], cs_coolorscodes, 0.0)

hdbclu <- hdbscan(cs_elements[,useful], minPts = 3)$cluster
points(tu[hdbclu > 0,], col = rainbow(length(unique(hdbclu)))[hdbclu[hdbclu>0]], cex = 1.2)


dev.off()
ce <- data.frame(cs_fields, cs_elements)
ucs <- unique(cs_fields)
meddf <- data.frame(t(sapply(1:length(ucs), function(i) pam(cs_elements[cs_fields==ucs[i],], k=1)$medoids)))
names(meddf) <- names(ce[,-1])
row.names(meddf) <- ucs

ameddf <- agnes(meddf, metric = "manhattan")

pdf("agnes_clus_useful.pdf")
plot(ameddf)

dev.off()

q()

pdf("tsne_allin.pdf")
allin <- data.frame(model.matrix( ~.^2, data=data.frame(scale(cs_elements[,useful])))[,-1]) #data.frame(scale(cs_elements[,!(names(cs_elements) %in% impo)]), model.matrix( ~.^3, data=data.frame(scale(cs_elements[,impo])))[,-1])
plot(tsne(allin, perplexity = 5), col = cs_coolors, pch = cs_pchs)

aridbscan(3, allin, cs_coolorscodes, 0.0)

dev.off()

q()
pdf("elements_tSNE.pdf")
ele_tSNE <- tsne(t(cs_elements), perplexity = 5)
plot(ele_tSNE, xlab = "X", ylab = "Y", type = "n")
text(ele_tSNE[,1], ele_tSNE[,2], names(cs_elements), cex = 0.5)

q()


pdf("dotchartelements.pdf")

eledot <- function(ele)
{
    medianel <- tapply(cs_elements[,ele], as.factor(as.character(cs_fields)), median) #median
    dotchart(cs_elements[,ele], pch = cs_pchs, pt.cex = 0.5, lcolor = "white", col = cs_coolors, xlab = ele, groups = as.factor(polish_names(as.character(cs_fields))))
}

sapply(names(cs_elements), eledot)

pdf("physics.pdf")
alpha_elements <-  paste(rep(c("MG", "TI", "SI", "CA"),2), rep(1:2, each = 4), sep = "")
cs_alpha_elements <- names(cs_elements)[names(cs_elements) %in% alpha_elements]
Fepeak_elements <- paste(rep(c("FE", "NI", "CO", "CR", "MN"),2), rep(1:2, each = 5), sep = "")
cs_Fepeak_elements <- names(cs_elements)[names(cs_elements) %in% Fepeak_elements]
neutroncapture_elements <- paste(rep(c("SR", "Y", "ZR", "BA", "LA", "ND", "EU"),2), rep(1:2, each = 7), sep = "")
cs_neutroncapture_elements <- names(cs_elements)[names(cs_elements) %in% neutroncapture_elements]
oddZelements <- paste(rep(c("NA", "AL", "K", "V", "CU", "SC"),2), rep(1:2, each = 6), sep = "")
cs_oddZelements <- names(cs_elements)[names(cs_elements) %in% oddZelements]

cs_alpha_elements
cs_Fepeak_elements
cs_neutroncapture_elements
cs_oddZelements

pairs(cs_elements[,cs_alpha_elements], col = cs_coolors, pch = cs_pchs)
pairs(cs_elements[,cs_Fepeak_elements], col = cs_coolors, pch = cs_pchs)
pairs(cs_elements[,cs_neutroncapture_elements], col = cs_coolors, pch = cs_pchs)
pairs(cs_elements[,cs_oddZelements], col = cs_coolors, pch = cs_pchs)

u_alpha <- prcomp(cs_elements[,cs_alpha_elements])$x[,2]
u_Fe <- prcomp(cs_elements[,cs_Fepeak_elements])$x[,2]
u_n <- prcomp(cs_elements[,cs_neutroncapture_elements])$x[,2]
u_Z <- prcomp(cs_elements[,cs_oddZelements])$x[,2]

pippo <- biclust(as.matrix(cs_elements), method=BCPlaid())
pippo
nrow(pippo@RowxNumber)
ncol(pippo@RowxNumber)
length(u_n)
length(u_Z)

plot(u_n, u_Z, col = cs_coolors, pch = cs_pchs, cex = 0.5)
points(u_n[pippo@RowxNumber[,1]], u_Z[pippo@RowxNumber[,1]], col = "black")
#points(u_n[pippo@RowxNumber[,2]], u_Z[pippo@RowxNumber[,2]], col = "#0412F0")
#points(u_n[pippo@RowxNumber[,3]], u_Z[pippo@RowxNumber[,3]], col = "#FFF000")


q()


pdf("allin.pdf") #now with logs
#impo <- c("Y2", "BA2", "FE2", "ZN1", "SC2", "AL1")
allin <- data.frame(model.matrix( ~.^2, data=data.frame(scale(cs_elements)))[,-1]) #data.frame(scale(cs_elements[,!(names(cs_elements) %in% impo)]), model.matrix( ~.^3, data=data.frame(scale(cs_elements[,impo])))[,-1])
plot(prcomp(allin)$x[,1:2], col = cs_coolors, pch = cs_pchs)
#plot(tsne(allin), col = cs_coolors, pch = cs_pchs)
#pairs(tsne(allin, k=3), col = cs_coolors, pch = cs_pchs)
umap.newconfig <- umap.defaults
umap.newconfig$n_components <- 4
ucs <- umap(allin, umap.newconfig)

pairs(ucs$layout, col = cs_coolors, pch = cs_pchs, cex = 0.5)

nrow(cs_elements)
ncol(cs_elements)
image(t(scale(cs_elements)[order(cs_coolorscodes),])) #order(cs_coolorcodes)

nrow(allin)
ncol(allin)
image(t(scale(allin)[order(cs_coolorscodes),]))

nrow(ucs$layout)
ncol(ucs$layout)
image(t(scale(ucs$layout)[order(cs_coolorscodes),]))

impot <- c("SI1", "AL1", "FE2", "SC2", "FE2.CO1", "CR2", "SI1.CU1", "CR1.CR2")
impoto <- c("SI1", "AL1", "FE2", "SC2", "CR2")

nrow(cs_elements[,impoto])
ncol(cs_elements[,impoto])
image(t(scale(cs_elements[,impoto])[order(cs_coolorscodes),])) #order(cs_coolorcodes)

nrow(allin[,impot])
ncol(allin[,impot])
image(t(scale(allin[,impot])[order(cs_coolorscodes),]))

pairs(log10(1+allin[,c("FE2.CO1", "SI1.CU1", "CR1.CR2")]), col = cs_coolors, pch = cs_pchs, cex = 0.5)

q()

aridbscan <- function(minPts, data, labels, pcut, fields=cs_fields)
{
    hdbresults <- hdbscan(data, minPts = minPts)
    dbscangroups <- hdbresults$cluster
    notnoise <- hdbresults$membership_prob > pcut
    ari <- adjustedRandIndex(dbscangroups[notnoise], labels[notnoise])
    diagnos <- c(ari, length(dbscangroups[notnoise])/length(dbscangroups), length(unique(labels[notnoise])), min(dbscangroups[notnoise]), length(unique(dbscangroups[notnoise])))
    return(list(diagnos=diagnos, tab = table(dbscangroups[notnoise], fields[notnoise])))
}

aridbscan(2, allin, cs_coolorscodes, 0.1)
unique(cs_fields)
cs_coolorscodes

q()

die <- dist(scale(cs_elements), method="minkowski", p=1.5)
hdbscan(die, 4)$cluster -> mu

mu

u_n <- cs_elements$BA2 - 0.2*cs_elements$ZR1
u_Z <- 0.7*cs_elements$SC1 - 0.5*cs_elements$SC2
pdf("agnesmax.pdf")
plot(u_n, u_Z, col = cs_coolors, pch = cs_pchs, cex = 0.5)
sapply(unique(mu), function(u) points(u_n[mu==u], u_Z[mu==u], col = rainbow(length(unique(mu)))[u+1]))

q()

calculateCorrelations <- function(field)
{
    df <- cs_elements[cs_fields==field,]
    return(quantile(cor(df), c(0.1, 0.5, 0.9)))
}

quantile(cor(cs_elements), c(0.1, 0.5, 0.9))
sapply(unique(cs_fields), calculateCorrelations)

q()

pdf("physics.pdf")
alpha_elements <-  paste(rep(c("MG", "TI", "SI", "CA"),2), rep(1:2, each = 4), sep = "")
cs_alpha_elements <- names(cs_elements)[names(cs_elements) %in% alpha_elements]
Fepeak_elements <- paste(rep(c("FE", "NI", "CO", "CR", "MN"),2), rep(1:2, each = 5), sep = "")
cs_Fepeak_elements <- names(cs_elements)[names(cs_elements) %in% Fepeak_elements]
neutroncapture_elements <- paste(rep(c("SR", "Y", "ZR", "BA", "LA", "ND", "EU"),2), rep(1:2, each = 7), sep = "")
cs_neutroncapture_elements <- names(cs_elements)[names(cs_elements) %in% neutroncapture_elements]
oddZelements <- paste(rep(c("NA", "AL", "K", "V", "CU", "SC"),2), rep(1:2, each = 6), sep = "")
cs_oddZelements <- names(cs_elements)[names(cs_elements) %in% oddZelements]

cs_alpha_elements
cs_Fepeak_elements
cs_neutroncapture_elements
cs_oddZelements

pairs(cs_elements[,cs_alpha_elements], col = cs_coolors, pch = cs_pchs)
pairs(cs_elements[,cs_Fepeak_elements], col = cs_coolors, pch = cs_pchs)
pairs(cs_elements[,cs_neutroncapture_elements], col = cs_coolors, pch = cs_pchs)
pairs(cs_elements[,cs_oddZelements], col = cs_coolors, pch = cs_pchs)

u_alpha <- prcomp(cs_elements[,cs_alpha_elements])$x[,2]
u_Fe <- prcomp(cs_elements[,cs_Fepeak_elements])$x[,2]
u_n <- prcomp(cs_elements[,cs_neutroncapture_elements])$x[,2]
u_Z <- prcomp(cs_elements[,cs_oddZelements])$x[,2]

pippo <- biclust(scale(as.matrix(cs_elements)), method=BCPlaid())
nrow(pippo@RowxNumber)
ncol(pippo@RowxNumber)
length(u_n)
length(u_Z)


plot(u_n, u_Z, col = cs_coolors, pch = cs_pchs, cex = 0.5)
points(u_n[pippo@RowxNumber[,1]], u_Z[pippo@RowxNumber[,1]], col = "black")
points(u_n[pippo@RowxNumber[,2]], u_Z[pippo@RowxNumber[,2]], col = "#0412F0")
points(u_n[pippo@RowxNumber[,3]], u_Z[pippo@RowxNumber[,3]], col = "#FFF000")

pairs(data.frame(u_alpha, u_n, u_Z), col = cs_coolors, pch = cs_pchs, cex = 0.5)

ce <- data.frame(cs_fields, cs_elements)

ucs <- unique(cs_fields)
meddf <- data.frame(t(sapply(1:length(ucs), function(i) pam(cs_elements[cs_fields==ucs[i],], k=1)$medoids)))
names(meddf) <- names(ce[,-1])
row.names(meddf) <- ucs

meddf
nrow(meddf)
ncol(meddf)

spcamed <- spca(meddf, 2, alpha = 0.005)
spcamed
summary(spcamed)
spcamed$loadings
spcamed$center
names(meddf)

plot(spcamed$scores, pch = 16)

ameddf <- agnes(meddf, metric = "manhattan")

pdf("agnes_clus.pdf")
plot(ameddf)
summary(ameddf)

spcacs_alpha <- robspca(cs_elements[,cs_alpha_elements], 2, alpha = 0.003)
spcacs_neutron <- robspca(cs_elements[,cs_neutroncapture_elements], 2, alpha = 0.003)
spcacs_oddZ <- robspca(cs_elements[,cs_oddZelements], 2, alpha = 0.003)

cs_alpha_elements
spcacs_alpha$loadings

cs_neutroncapture_elements
spcacs_neutron$loadings

cs_oddZelements
spcacs_oddZ$loadings

u_alpha <- -0.5*cs_elements$SI1 + 0.4*cs_elements$CA1 + 0.5*cs_elements$TI1 - 0.3*cs_elements$TI2
u_n <- cs_elements$BA2 - 0.2*cs_elements$ZR1
u_Z <- 0.7*cs_elements$SC1 - 0.5*cs_elements$SC2

pairs(data.frame(u_alpha, u_n, u_Z), col = cs_coolors, pch = cs_pchs, cex = 0.5)

plot(tsne(cs_elements[,cs_alpha_elements]), col = cs_coolors, pch = cs_pchs)
plot(tsne(cs_elements[,cs_Fepeak_elements]), col = cs_coolors, pch = cs_pchs)
plot(tsne(cs_elements[,cs_neutroncapture_elements]), col = cs_coolors, pch = cs_pchs)
plot(tsne(cs_elements[,cs_oddZelements]), col = cs_coolors, pch = cs_pchs)

plot(cs_elements$SC1, cs_elements$SC2, col = cs_coolors, pch = cs_pchs)
abline(0,1)
plot(cs_elements$ZR1 - cs_elements$ZR2, cs_elements$SC1-cs_elements$SC2, col = cs_coolors, pch = cs_pchs)

q()

pdf("allin.pdf") #now with logs
#impo <- c("Y2", "BA2", "FE2", "ZN1", "SC2", "AL1")
allin <- data.frame(model.matrix( ~.^2, data=data.frame(scale(cs_elements)))[,-1]) #data.frame(scale(cs_elements[,!(names(cs_elements) %in% impo)]), model.matrix( ~.^3, data=data.frame(scale(cs_elements[,impo])))[,-1])
#plot(prcomp(allin)$x[,1:2], col = cs_coolors, pch = cs_pchs)
#pairs(tsne(allin, k=3), col = cs_coolors, pch = cs_pchs)
#plot(umap(allin)$layout, col = cs_coolors, pch = cs_pchs)
nrow(allin)
nrow(cs_elements)
length(cs_fields)


impot <- c("SI1", "AL1", "FE2", "SC2")#, "FE2.CO1", "CR2", "SI1.CU1", "CR1.CR2")
pairs(allin[,impot], col = cs_coolors, pch = cs_pchs, cex = 0.5)
pairs(prcomp(allin[,impot])$x, col = cs_coolors, pch = cs_pchs)

q()

library("iml")
treemodel <- C5.0(as.factor(cs_fields) ~ ., data = allin)
predictor <- Predictor$new(treemodel, data = allin, y = cs_fields)
imp <- FeatureImp$new(predictor, loss = "ce")

imp$results

plot(imp)

q()

pdf("permutationimportanceandcrosspredictability.pdf")

element_importance <- imp$results[,3]

template <- "summary(lm(placeholder ~ ., data = cs_elements))$adj.r.squared"
lmi <- function(i) eval(parse(text = gsub("placeholder",imp$results[i,1],template)))

crosspredictability <- sapply(1:nrow(imp$results), lmi)

data.frame(imp$results[,1], crosspredictability, imp$results[,3]) #the lower the better

plot(crosspredictability, imp$results[,3], pch = 16, cex = 0.7, xlab = "cross predictability (adj. R squared)", ylab = "Permutation importance", type = "n")
text(crosspredictability, imp$results[,3], imp$results[,1], col="#A0A0A0", cex = 0.5)

pdf("important.pdf")

impo <- c("Y2", "BA2", "FE2", "ZN1", "SC2", "AL1")
simpo <- scale(cs_elements[,impo], center = TRUE, scale = TRUE)
plot(tsne(simpo), col = cs_coolors, pch = cs_pchs)
plot(tsne(cs_elements[,impo]), col = cs_coolors, pch = cs_pchs)
plot(umap(simpo)$layout, col = cs_coolors, pch = cs_pchs)
plot(umap(cs_elements[,impo])$layout, col = cs_coolors, pch = cs_pchs)
q()

plot(cs_elements[,c("Y2", "BA2")], col = cs_coolors, pch = cs_pchs)
plot(cs_elements[,c("Y2", "FE2")], col = cs_coolors, pch = cs_pchs)
plot(cs_elements[,c("BA2", "FE2")], col = cs_coolors, pch = cs_pchs)
plot(cs_elements[,c("ZN1", "Y2")], col = cs_coolors, pch = cs_pchs)
plot(cs_elements[,c("ZN1", "BA2")], col = cs_coolors, pch = cs_pchs)
plot(cs_elements[,c("ZN1", "FE2")], col = cs_coolors, pch = cs_pchs)
plot(cs_elements[,c("SC2", "BA2")], col = cs_coolors, pch = cs_pchs)
Fe <- cs_elements$FE1 + cs_elements$FE2
plot(cs_elements$SC2/Fe, cs_elements$BA2/Fe, col = cs_coolors, pch = cs_pchs)
plot(cs_elements[,c("AL1", "BA2")], col = cs_coolors, pch = cs_pchs)
plot(cs_elements$AL1/Fe, cs_elements$BA2/Fe, col = cs_coolors, pch = cs_pchs)
sc <- cs_elements[,"SC2"] - 2*cs_elements[,"SC1"]
plot(sc, cs_elements[,"BA2"], col = cs_coolors, pch = cs_pchs)
plot(cs_elements$ZR2, cs_elements$BA2, col = cs_coolors, pch = cs_pchs)
plot(cs_elements$ZR2/Fe, cs_elements$BA2/Fe, col = cs_coolors, pch = cs_pchs)

#divide by Fe?
cs_elements[,c("ZR2", "BA2", "SC2", "FE2")]


q()
pdf("dotchartelements.pdf")

medianel <- tapply(cs_elements[,"BA2"], as.factor(as.character(cs_fields)), median) #median
dotchart(cs_elements[,"BA2"], pch = cs_pchs, pt.cex = 0.5, lcolor = "white", col = cs_coolors, xlab = "BA2")


anyNA(cs_elements)

cbind(cs_fields, cs_elements[,"BA2"])

medianel





q()
aridbscan <- function(minPts, data, labels, pcut, fields=cs_fields)
{
    hdbresults <- hdbscan(data, minPts = minPts)
    dbscangroups <- hdbresults$cluster
    notnoise <- hdbresults$membership_prob > pcut
    ari <- adjustedRandIndex(dbscangroups[notnoise], labels[notnoise])
    diagnos <- c(ari, length(dbscangroups[notnoise])/length(dbscangroups), length(unique(labels[notnoise])), min(dbscangroups[notnoise]), length(unique(dbscangroups[notnoise])))
    return(list(diagnos=diagnos, tab = table(dbscangroups[notnoise], fields[notnoise])))
}

important <- c("CR2", "AL1", "SC2", "FE2")
importantish <- c("CR2", "AL1", "SC2", "FE2", "SC1", "Y2", "CE2", "NI1", "EU2", "LA2", "MG1")

pcuts <- (0:40)/40
df_space <- data.frame(pcuts, t(sapply(pcuts, function(pcut) aridbscan(2, cs_elements, cs_coolorscodes, pcut)$diagnos)))
df_space
umap.newconfig <- umap.defaults
umap.newconfig$alpha <- 0.1
umap.newconfig$n_components <- 4
ucs <- umap(cs_elements, umap.newconfig)
df_umap <- data.frame(pcuts, t(sapply(pcuts, function(pcut) aridbscan(2, ucs$layout, cs_coolorscodes, pcut)$diagnos)))
df_umap

pdf("umappa.pdf")
pairs(ucs$layout, col = cs_coolors, pch = cs_pchs)

pdf("tsne.pdf")
#plot(tsne(cs_elements), col = cs_coolors, pch = cs_pchs)
ts <- tsne(cs_elements, k = 4)
pairs(ts, col = cs_coolors, pch = cs_pchs)

df_tsne <- data.frame(pcuts, t(sapply(pcuts, function(pcut) aridbscan(2, ts, cs_coolorscodes, pcut)$diagnos)))
df_tsne

q()
#aridbscan(2, cs_elements, cs_coolorscodes, 0.0)$tab



#aridbscan(2, cs_elements[,importantish], cs_coolorscodes)
#aridbscan(2, cs_elements[,important], cs_coolorscodes)

q()

pdf("important.pdf")
important <- c("CR2", "AL1", "SC2", "FE2")
importantish <- c("CR2", "AL1", "SC2", "FE2", "SC1", "Y2", "CE2", "NI1", "EU2", "LA2", "MG1")

primp <- prcomp(cs_elements[,important])
primp

pairs(primp$x[,1:3], pch = cs_pchs, col = cs_coolors)

spcacs <- robspca(cs_elements[,important], 2, alpha = 0.003)

spcacs
spcacs$loadings

plot(spcacs$scores, pch = cs_pchs, col = cs_coolors)

plot(tsne(cs_elements[,important]), pch = cs_pchs, col = cs_coolors)

pdf("factor.pdf")
library("psych")
trf <- fa(cs_elements[,importantish], nfactors = 2,rotate = "oblimin", fm="minres")
print(trf)
print(trf$loadings,cutoff = 0.3)

plot(trf$scores, pch = cs_pchs, col = cs_coolors)

q()

pdf("residuals.pdf")
a <- lm(CR2 ~ ., data = cs_elements)$residuals
b <- lm(AL1 ~ ., data = cs_elements)$residuals

plot(a,b, pch = cs_pchs, col = cs_coolors)

pdf("prcomptest.pdf")
plot(prcomp(cs_elements)$x[,1:2], pch = cs_pchs, col = cs_coolors)
important <- c("CR2", "AL1", "SC2", "FE2")
plot(prcomp(cs_elements[,important])$x[,1:2], pch = cs_pchs, col = cs_coolors)

spcacs <- robspca(cs_elements, 2, alpha = 0.003)

plot(spcacs$scores, col = cs_coolors, pch = cs_pchs)
q()

rep(F, length(as.character(cs_fields))) -> holdout
for(field in unique(as.character(cs_fields)))
{
    a <- holdout[as.character(cs_fields)==field]
    ra <- rep(c(T,F,F), ceiling(length(a)/3))[1:length(a)]
    holdout[as.character(cs_fields)==field] <- sample(ra, length(ra), rep = FALSE)
}

mahala <- scale(prcomp(cs_elements[,importantish])$x, center = TRUE, scale = TRUE)
cs_elements <- mahala #cs_elements#[,important] #togli, metti
#cs_elements <- scale(cs_elements, center = TRUE, scale = TRUE)

train <- cs_elements[!holdout,]
train_codes <- cs_coolorscodes[!holdout]
test <- cs_elements[holdout,]
test_codes <- cs_coolorscodes[holdout]

aridbscan <- function(eps, minPts, data, labels)
{
    dbscangroups <- dbscan(data, eps = eps, minPts = minPts)$cluster
    ari <- adjustedRandIndex(dbscangroups, labels)
    return(ari)
}

optf <- function(minPts) optimize(f = function(eps) aridbscan(eps, minPts, train, train_codes), interval = c(0.01, 5.0), maximum = TRUE)

sapply(2:20, optf)

aridbscan(0.7, 4, test, test_codes)


q()

library("sparsepca")
spcacs <- spca(cs_elements, 3, alpha = 0.01)

spcacs
summary(spcacs)

spcacs$loadings

ce <- data.frame(cs_fields, cs_elements)

ucs <- unique(cs_fields)
meddf <- data.frame(t(sapply(1:length(ucs), function(i) pam(cs_elements[cs_fields==ucs[i],], k=1)$medoids)))
names(meddf) <- names(ce[,-1])
row.names(meddf) <- ucs

meddf
nrow(meddf)
ncol(meddf)

spcamed <- spca(meddf, 3, alpha = 0.005)
spcamed
summary(spcamed)
spcamed$loadings
spcamed$center
names(meddf)

plot(spcamed$scores, pch = 16)

u <- -0.04*(cs_elements[,3] - spcamed$center[3]) - 0.51*(cs_elements[,14] - spcamed$center[14])
v <- 0.96*(cs_elements[,5] - spcamed$center[5]) + 0.62*(cs_elements[,15] - spcamed$center[15])

plot(u,v, col=cs_coolors, pch = cs_pchs)

q()

ameddf <- agnes(meddf, metric = "manhattan")

pdf("agnes_clus.pdf")
plot(ameddf)
summary(ameddf)

ucs[cutree(ameddf, 3) == 1]
ucs[cutree(ameddf, 3) == 2]
ucs[cutree(ameddf, 3) == 3]


cf <- as.character(cs_fields)
cf[cs_fields %in% ucs[cutree(ameddf, 3) == 2]] <- "T_1"
cf[cs_fields %in% ucs[cutree(ameddf, 3) == 3]] <- "T_2"

ceg <- data.frame(cf, cs_elements[,c("CR2", "AL1", "SC2")])
ceg

ugg <- unique(cf)
ceddf <- data.frame(t(sapply(1:length(ugg), function(i) pam(cs_elements[cf==ugg[i],], k=1)$medoids)))
names(ceddf) <- names(ceg[,-1])
row.names(ceddf) <- ugg

ceddf

library("C50")
tree_ele <- C5.0(cf ~ ., data = ceg, rules = TRUE)

summary(tree_ele)
tree_ele

library('pre')
pre(cf ~ ., data = ceg)

library("iml")
tree_ce <- C5.0(cs_fields ~ ., data = ce, trials = 10)
predictor <- Predictor$new(tree_ce, data = ce[,-1], y = ce$cs_fields)
imp <- FeatureImp$new(predictor, loss = "ce")

imp

pdf("permutationimportance.pdf")
plot(imp)

pdf("crosspredictability.pdf")
template <- "summary(lm(placeholder ~ ., data = cs_elements))$adj.r.squared"
lmi <- function(i) eval(parse(text = gsub("placeholder",names(cs_elements)[i],template)))

crosspredictability <- sapply(1:ncol(cs_elements), lmi)

data.frame(names(cs_elements), crosspredictability) #the lower the better

q()

important <- c("CR2", "AL1", "SC2", "FE2")
importantish <- c("CR2", "AL1", "SC2", "FE2", "SC1", "Y2", "CE2", "NI1", "EU2", "LA2", "MG1")


pairs(cs_elements[,important[1:3]], col=cs_coolors, pch = cs_pchs)

plot(prcomp(cs_elements[,important])$x[,1:2], col=cs_coolors, pch = cs_pchs)

library("ica")
plot(icafast(cs_elements[,important], 2)$S, col=cs_coolors, pch = cs_pchs)

plot(umap(cs_elements[,important])$layout, col=cs_coolors, pch = cs_pchs)

#plot(tsne(cs_elements[,important]), col=cs_coolors, pch = cs_pchs)
#plot(tsne(cs_elements[,importantish]), col=cs_coolors, pch = cs_pchs)

pdf("prcomp_permutationimportance.pdf")

prcs <- prcomp(cs_elements[,importantish])
prcs$rotation

cepr <- data.frame(cs_fields, prcs$x)

tree_cepr <- C5.0(cs_fields ~ ., data = cepr, trials = 10)
predictorpr <- Predictor$new(tree_cepr, data = cepr[,-1], y = cepr$cs_fields)
imp <- FeatureImp$new(predictorpr, loss = "ce")

imp
plot(imp)

table(cs_coolorscodes)
table(cs_fields)
sum(table(cs_fields))

rep(F, length(as.character(cs_fields))) -> holdout
for(field in unique(as.character(cs_fields)))
{
    a <- holdout[as.character(cs_fields)==field]
    ra <- rep(c(T,F,F), ceiling(length(a)/3))[1:length(a)]
    holdout[as.character(cs_fields)==field] <- sample(ra, length(ra), rep = FALSE)
}

cs_elements <- cs_elements#[,important] #togli, metti
#cs_elements <- scale(cs_elements, center = TRUE, scale = TRUE)

train <- cs_elements[!holdout,]
train_codes <- cs_coolorscodes[!holdout]
test <- cs_elements[holdout,]
test_codes <- cs_coolorscodes[holdout]

aridbscan <- function(eps, minPts, excludedElement, data, labels)
{
    if(excludedElement==0) dbscangroups <- hdbscan(data, minPts = minPts)$cluster
    if(excludedElement>0) dbscangroups <- hdbscan(data[,-excludedElement], minPts = minPts)$cluster
    ari <- adjustedRandIndex(dbscangroups, labels)
    return(ari)
}

sapply(2:20, function(i) aridbscan("no", i, 0, train, train_codes))
sapply(2:20, function(i) aridbscan("no", i, 0, test, test_codes))

q()

Ntry <- 60
epses <- runif(Ntry)
minPtses <- sample(2:12, Ntry, replace = TRUE)
excludedElements <- rep(0, Ntry) #sample(0:ncol(train), Ntry, replace = TRUE)
aries <- sapply(1:Ntry, function(i) aridbscan(epses[i], minPtses[i], excludedElements[i], train, train_codes))
resultdf <- data.frame(epses, minPtses, excludedElements, aries)
o <- order(aries)
tail(resultdf[o,])

aridbscan((resultdf[o,1])[Ntry], (resultdf[o,2])[Ntry], (resultdf[o,3])[Ntry], train, train_codes)
aridbscan((resultdf[o,1])[Ntry], (resultdf[o,2])[Ntry], (resultdf[o,3])[Ntry], test, test_codes)





q()

aripam <- function(k)
{
    pamgroups <- pam(cs_elements, k = k, metric = "manhattan")$clustering
    ari <- adjustedRandIndex(pamgroups, coolorscodes[completestars])
    return(ari)
}

aripams <- sapply(1:3, aripam)
aripams
which.max(aripams)

pdf("pairs.pdf")
pairs(cs_elements, col=cs_coolors, pch = cs_pchs)

dotsne <- TRUE #FALSE #

if(dotsne)
{
    tsnecs <- tsne(cs_elements, perplexity = 20)
    pdf("tsne.pdf")
    plot(tsnecs, col = cs_coolors, pch = cs_pchs)
}

umapcs <- umap(cs_elements)
umapcs
summary(umapcs)
umapcs$layout
pdf("umap.pdf")
plot(umapcs$layout, xlab = expression(U[1]), ylab = expression(U[2]), col = cs_coolors, pch = cs_pchs)

q()

agcs <- agnes(cs_elements, method = "ward")

agcs
summary(agcs)
k <- 11 #this is cheating already, you don't know the real number of groups
cutagcs <- cutree(agcs, k)

if(dotsne)
{
    pdf("agnes.pdf")
    plot(tsnecs, col = rainbow(k)[cutagcs], pch = pchs)
}

q()

A <- table(cutagcs, coolorscodes[completestars]) #compare the unsupervised classification with reality
#many groups will coincide, but by the way they are coded they do not end up on the diagonal
#in the following I recode the groups (permutation of the labels) so the maxima end up on the diagonal



table(recodedcutagcs, cutagcs) #this is just checking that the permutation is really a permutation

B <- table(recodedcutagcs, coolorscodes[completestars]) #this is the interesting table

B

sum(sapply(1:nrow(B), function(i) B[i,i]))/sum(B) #fraction of correctly classified stars

#we can do better! Use adjusted rand index from mclust, remember to cite citation(mclust)
#note that the adjusted Rand index is ~0 if independent classification, ~1 if same
adjustedRandIndex(cutagcs, coolorscodes[completestars]) #should be the same
adjustedRandIndex(recodedcutagcs, coolorscodes[completestars]) #as this one
#btw the adjusted Rand index can be calculated even if the number of groups is different

nrow(cs_elements)




















q()
cs_ids <- ids[completestars]
cs_groups <- groups[completestars]
cs_snr <- anto$SNR[completestars]
cs_feh <- anto$FEH[completestars]

nrow(cs_elements)

library("MASS")
pdf("parcoord_onlyms_radialvelselecttsne_3sigma.pdf")
parcoord(cs_elements[,-1], group_colors[cs_group_by_numeric_id])

#library("umap")
#tryumap <- umap(cs_elements[,-1])
#tryumap
#summary(tryumap)

Nsoughtgroups <- 12

library("cluster")
agel <- agnes(cs_elements[,-1], method = "ward")
cutree(agel, Nsoughtgroups) -> cluagel

clupamel <- pam(cs_elements[,-1], Nsoughtgroups)$clustering

pdf("knndistplot.pdf")
kNNdistplot(cs_elements[,-1])

dbscan(cs_elements[,-1], eps = 0.15, minPts = 4)$cluster -> cludbscan

library("tsne")
tsne(cs_elements[,-1], perplexity = 50) -> p50tsne

#p50tsne <- tryumap$layout

pdf("DBSCAN+TSNEonlyms_radialvelselect_3sigma.pdf")
pchs <- rep(c(0:6,8,15:18), ceiling(length(group_colors)/11.0))[1:length(group_colors)]
#cexes <- rep(0.7, ) #1 + 2*(log10(cs_snr)-min(log10(cs_snr)))/(max(log10(cs_snr))-min(log10(cs_snr)))
plot(p50tsne, xlab = "x", ylab = "y", pch = pchs[cs_group_by_numeric_id], col = group_colors[cs_group_by_numeric_id], cex = 0.7)
legend(x="topright", legend=unique(groups), col=group_colors, cex = 0.655, pch = pchs)

plot(p50tsne, xlab = "x", ylab = "y", pch = pchs[cs_group_by_numeric_id], col = group_colors[cludbscan+1], cex = 0.7)
plot(p50tsne, xlab = "x", ylab = "y", pch = pchs[cs_group_by_numeric_id], col = group_colors[cluagel], cex = 0.7)
plot(p50tsne, xlab = "x", ylab = "y", pch = pchs[cs_group_by_numeric_id], col = group_colors[clupamel], cex = 0.7)


nrow(cs_elements)
ncol(cs_elements)


